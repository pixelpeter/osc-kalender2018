<?php namespace App\Services;

use Auth;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\WebProcessor;

class PaymentLog
{
    /**
     * write
     * @return void
     */
    public function write($desc='', $data='')
    {
        // logger instance
        $log = new Logger('PaymentLog');
        // handler init, making days separated logs
        $handler = new RotatingFileHandler(storage_path('/logs/payment.log'), 0, Logger::INFO);
        // formatter, ordering log rows
        $handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message% %extra% %context%\n"));
        // add handler to the logger
        $log->pushHandler($handler);
        // processor, adding URI, IP address etc. to the log
        $log->pushProcessor(new WebProcessor);
        // processor, memory usage
        $log->pushProcessor(new MemoryUsageProcessor);
        // custom: I add user id and permission group (by Sentry) to log
        $user = Auth::id();

        $log->addInfo('USER ID: ' . $user);
        $log->addInfo("{$desc}: {$data}");
    }
}