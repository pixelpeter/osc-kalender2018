<?php namespace App\Services;

use App\Models\Photo;
use Intervention\Image\ImageManager as Image;

class ThumbnailMaker
{
    const DIR_ORG = "/uploads/org/";
    const DIR_THUMBNAIL = "/uploads/thumbnail/";
    const DIR_DETAIL = "/uploads/detail/";

    const WIDTH_THUMBNAIL = 400;
    const HEIGHT_THUMBNAIL = 300;
    const WIDTH_DETAIL = 800;
    const HEIGHT_DETAIL = 600;
    /**
     * @var Photo
     */
    private $photo;

    /**
     * @param Image $image
     */
    public function __construct(Image $image, Photo $photo)
    {
        $this->image = $image;
        $this->photo = $photo;
    }

    public function makeThumbnail($imageName)
    {
        $this->image->make(public_path() . self::DIR_ORG . $imageName)
            ->resize(self::WIDTH_THUMBNAIL, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->crop(self::WIDTH_THUMBNAIL, self::HEIGHT_THUMBNAIL)
            ->save(public_path() . self::DIR_THUMBNAIL . $imageName)
            ->destroy();
    }

    public function makeDetail($imageName)
    {
        $this->image->make(public_path() . self::DIR_ORG . $imageName)
            ->resize(null, self::HEIGHT_DETAIL, function ($constraint) {
                $constraint->aspectRatio();
            })
            // ->crop(self::WIDTH_DETAIL, self::HEIGHT_DETAIL)
            ->save(public_path() . self::DIR_DETAIL . $imageName)
            ->destroy();
    }

    public function generate($imageName)
    {
        $this->makeThumbnail($imageName);

        $this->makeDetail($imageName);
    }

    public function generateAll()
    {
        $images = $this->photo->all();

        foreach($images as $image)
        {
            $this->generate($image->name);
        }
    }

}