<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;
/**
 * UserLog Facade
 *
 * @author     Barna Szalai <sz.b@devartpro.com>
 */
class PaymentLog extends Facade {
    protected static function getFacadeAccessor()
    {
        return \App\Services\PaymentLog::class;
    }
}