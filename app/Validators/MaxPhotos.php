<?php namespace App\Validators;

use App\Models\Photo;
use Illuminate\Support\Facades\Auth;

class MaxPhotos
{
    public function validate($attribute, $value, $parameters)
    {
        if (!Auth::user()) {
            return;
        }

        $count = count(Photo::where('member_id', Auth::id())->get());

        return $count < $parameters[0];
    }

    public function message($message, $attribute, $rule, $parameters)
    {
        return str_replace(":max", $parameters[0], $message);
    }
}