<?php

// PAGES
Route::get('/', 'PagesController@home');

// AUTHENTICATION
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');


// TEST
/*
Route::get('/test', function () {
    dd( \App\Models\Member::first());
    $t = \App\Models\OscMember::with('address')->first();
    var_dump( $t, $t->address->street );
    // return \App\Models\DnogtMember::first();
    // return App\Pic::all()->take(10);
});
*/

// UPLOADS
// sRoute::resource('upload','UploadsController');

// PHOTOS (admin)
Route::resource('photos','PhotosController');

// RESULT (admin)
Route::get('result','ResultController@index');
Route::get('censor/{picId}/{censor}','ResultController@censor');
Route::get('censored','ResultController@censored');

// VOTES
//Route::get('votes','VotesController@index');
//Route::get('votes/save/{id}','VotesController@save');
//Route::get('votes/done','VotesController@done');

// ORDER
//Route::get('order','OrderController@index');
//Route::post('order/store','OrderController@store');
//Route::get('order/show','OrderController@show');
//Route::post('order/save','OrderController@save');
//Route::post('order/update', 'OrderController@update');
//Route::get('order/result/{id}', 'OrderController@result');
//Route::get('order/finished/{id}', 'OrderController@finished');
//Route::get('order/failed', 'OrderController@failed');
//Route::get('order/canceled', 'OrderController@canceled');

// PAYMENT
//Route::get('payment/create','PaypalPaymentController@create');
//Route::get('payment/execute','PaypalPaymentController@execute');
//Route::get('payment/cancel','PaypalPaymentController@cancel');
//Route::get('payment/result','PaypalPaymentController@result');
//Route::get('payment/{id}','PaypalPaymentController@show');

// PAYMENTS
//Route::resource('payments','PaymentsController');

// TEST
// Route::get('/test/test', 'TestController@index');
