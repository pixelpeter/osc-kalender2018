<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::select(DB::raw('
            kalender2018_photos.id,
            kalender2018_photos.name,
            SUM(kalender2018_votes.vote) AS vote,
            COUNT(*) AS count')
        )
        ->join('kalender2018_votes','kalender2018_photos.id', '=', 'kalender2018_votes.photo_id')
        ->where('is_censored','=',0)
        ->groupBy('kalender2018_votes.photo_id')
        ->orderBy('vote','DESC')
        ->orderBy('count','DESC')
        ->get();

        return view('result.index', compact('photos'));
    }

    public function censor($picId,$censor)
    {
        $photo = Photo::where('id', $picId)->first();
        $photo->is_censored = $censor;
        $photo->save();

        return back();
    }

    public function censored()
    {
        $photos = Photo::select(DB::raw('
            kalender2018_photos.id,
            kalender2018_photos.name,
            SUM(kalender2018_votes.vote) AS vote,
            COUNT(*) AS count')
        )
            ->join('kalender2018_votes','kalender2018_photos.id', '=', 'kalender2018_votes.photo_id')
            ->where('is_censored','!=',0)
            ->groupBy('kalender2018_votes.photo_id')
            ->orderBy('vote','DESC')
            ->orderBy('count','DESC')
            ->get();

        return view('result.censored', compact('photos'));
    }

}
