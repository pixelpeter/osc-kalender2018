<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Photo;
use App\Services\ThumbnailMaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UploadsController extends Controller
{
    /**
     * @var ThumbnailMaker
     */
    private $thumbnailMaker;

    /**
     * @param ThumbnailMaker $thumbnailMaker
     */
    public function __construct(ThumbnailMaker $thumbnailMaker)
    {
        $this->middleware('auth');

        $this->thumbnailMaker = $thumbnailMaker;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Photo $photo)
    {
        $photos = $photo->where('member_id',Auth::id())->get();

        return view('uploads.form', compact('photos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( $response = $this->isValidUpload() ) {
            return $response;
        }

        $filename = $this->makeFilename($request);

        $this->savePhoto($request, $filename);

        $this->thumbnailMaker->generate($filename);
    }

    public function destroy($id)
    {
        $photo = Photo::where('id', $id)->where('member_id', Auth::id())->first();

        if ( $photo ) {
            unlink(public_path('uploads/thumbnail/').$photo->name);
            unlink(public_path('uploads/org/').$photo->name);
            unlink(public_path('uploads/detail/').$photo->name);

            Photo::destroy($id);
        }

        return back();
    }

    /**
     * Validate the upload
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function isValidUpload()
    {
        $input = Input::all();
        $rules = [
            'file' => 'required|mimes:jpg,jpeg,png|maxPhotos:5|image_size:>=2500,>=1500'
        ];

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return response($validation->errors()->first(), 403);
        }

        return '';
    }

    /**
     * Make a filename for the uploaded file
     *
     * @param Request $request
     * @return string $filename
     */
    protected function makeFilename(Request $request)
    {
        $file = $request->file('file');
        $date = microtime(true);
        $userId = Auth::id();

        $filename = sprintf("%s_%s.%s",
            $date,
            $userId,
            $file->getClientOriginalExtension()
        );

        return $filename;
    }

    /**
     * Persist the Photo to the database and save it to the file system
     *
     * @param Request $request
     * @param $filename
     * @return void
     */
    protected function savePhoto(Request $request, $filename)
    {
        $file = $request->file('file');

        $file->move(public_path() . '/uploads/org/', $filename);

        $photo = new Photo([
            'name' => $filename,
            'member_id' => Auth::id(),
        ]);

        $photo->save();
    }
}
