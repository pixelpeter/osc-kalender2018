<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Order;
use App\Models\OscAddress;
use App\Models\OscMember;
use Auth;
use Illuminate\Http\Request;
use Input;
use Mail;
use Session;

class OrderController extends Controller
{
    /**
     * @var OscMember
     */
    private $member;
    /**
     * @var OscAddress
     */
    private $address;
    /**
     * @var Order
     */
    private $order;

    /**
     * @param OscMember $member
     * @param OscAddress $address
     */
    public function __construct(OscMember $member, OscAddress $address, Order $order)
    {
        $this->middleware('auth');

        $this->member = $member;
        $this->address = $address;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member = $this->member->with('address')->where('id', Auth::id())->first();
        $countries = config('countries');

        return view('order.index', compact('member', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $address = $this->address->where('member_id', Auth::id())->first();
        $address->update(Input::all());

        $member = $this->member->where('id', Auth::id())->first();
        $member->email = Input::get('email');
        $member->save();

        Session::put('country', $address->country);
        Session::put('sku', 1);

        return redirect('order/show');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $product = config('calendarapp.product');
        $price = config('calendarapp.price');
        $product_total = $this->calculateProductTotal();

        $shipping = $this->calculateShipping();
        $total = $this->calculateTotal();

        $product_wt = number_format(round($product*1.19,2),2);
        $price_wt = number_format(round($price*1.19,2),2);
        $product_total_wt = number_format($product_total*1.19,2);
        $shipping_wt = number_format(round($shipping*1.19,2),2);
        $total_wt = number_format(round($total*1.19,2),2);

        $tax = ($product_total+$shipping)*0.19;
        $tax = number_format(round($tax,2),2);

        return view('order.show', compact(
            'product', 'price', 'product_total', 'shipping', 'total',
            'product_wt', 'price_wt', 'product_total_wt', 'shipping_wt', 'total_wt',
            'tax'
            )
        );
    }

    public function update()
    {
        Session::put('sku', Input::get('sku'));

        if (Input::get('update')) {
            return redirect('order/show');
        }

        return $this->create();
    }

    public function create()
    {
        $member = $this->member->with('address')->find(Auth::id());

        $order = $this->order->create([
            'member_id' => Auth::id(),
            'company' => $member->address->company,
            'first_name' => $member->address->first_name,
            'last_name' => $member->address->last_name,
            'street' => $member->address->street,
            'zip' => $member->address->zip,
            'city' => $member->address->city,
            'country' => $member->address->country,
            'email' => $member->email,
            'count' => Input::get('sku'),
            'shipping' => $this->calculateShipping(),
            'product_total' => $this->calculateProductTotal(),
            'total' => $this->calculateTotal(),
        ]);

        Session::put('order_id',$order->id);

        return redirect('payment/create');
    }


    public function result($id)
    {
        $member = $this->member
            ->with('address', 'order')
            ->where('id',Auth::id())
            ->first();

        $order = $this->order
            ->where('member_id', Auth::id())
            ->where('payment_id', $id)
            ->where('payment_status', 'approved')
            ->first();

        if (! $order)
        {
            return redirect("/");
        }

        $product = config('calendarapp.product');

        $tax = $order->total*0.19;
        $total_wt = $order->total*1.19;
        $country = config('countries')[$order->country];
        $state = $order->payment_status;

        if ( $order && $state == 'approved')
        {
            Mail::send('emails.order.success', compact('member', 'order', 'product', 'tax', 'total_wt', 'country'), function ($message) use($order) {
                $message->from(config('calendarapp.email_from'));
                $message->to($order->email)->bcc(config('calendarapp.email_from'));
                $message->subject("Kalenderaktion 2018: Bestellbestätigung [{$order->id}]");
            });

            return redirect('order/finished/'. $id);
        }
        else
        {
            Mail::send('emails.order.error', compact('member', 'order'), function ($message) use($order) {
                $message->from(config('calendarapp.email_from'));
                $message->to($order->email)->bcc(config('calendarapp.email_from'));
                $message->subject("Kalenderaktion 2018: Probleme bei der Bestellung [{$order->id}]");
            });

            return redirect('order/failed');
        }
    }

    public function finished($id)
    {
        $order = $this->order
            ->where('member_id', Auth::id())
            ->where('payment_id', $id)
            ->where('payment_status', 'approved')
            ->first();

        if (! $order)
        {
            return redirect("/");
        }

        $state = $order->payment_status;

        return view('order.finished', compact('state'));
    }

    public function failed()
    {
        $message = "Es gab ein Problem bei der Transaktion<br>Die Bestellung konnte <b>NICHT</b> erfolgreich abgeschlossen werden";

        return view('order.failed', compact('message'));
    }

    public function canceled()
    {
        return view('order.canceled');
    }

    protected function calculateProductTotal()
    {
        $price = config('calendarapp.price');

        return \Session::get('sku') * $price;
    }

    protected function calculateTotal()
    {
        $shipping = $this->calculateShipping();
        $product = $this->calculateProductTotal();

        return $shipping + $product;
    }

    protected function calculateShipping()
    {
        $shipping_costs = config('calendarapp.shipping');

        switch (Session::get('country')) {
            case 'DE':
                $shipping = $shipping_costs['DE'][Session::get('sku')];
                break;

            case 'CH':
                $shipping = $shipping_costs['CH'][Session::get('sku')];
                break;

            default:
                $shipping = $shipping_costs['EU'][Session::get('sku')];
        }

        return $shipping;
    }
}
