<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Photo;
use App\Models\Vote;
use \Debugbar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class VotesController extends Controller
{
    /**
     * @var Photo
     */
    protected $photo;
    /**
     * @var Vote
     */
    protected $vote;

    /**
     * @param Photo $photo
     * @param Vote $vote
     * @internal param Vote $votes
     */
    public function __construct(Photo $photo, Vote $vote)
    {
        $this->middleware('auth');

        \DebugBar::disable();

        $this->photo = $photo;
        $this->vote = $vote;
    }

    /**
     * Display a random unvoted single photo.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $photoCount = $this->photo->count();
            $votesCount = $this->vote->where('member_id',Auth::id())->count();
            $photo = $this->photo->notVoted();
        }
        catch(\Exception $e) {
            return redirect('votes/done');
        }

        return view('votes.index', compact('photo', 'photoCount', 'votesCount'));
    }

    /**
     * Save the vote for the current user and current photo
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save($id)
    {
        $this->vote->create(
            Crypt::decrypt($id)
        );

        return redirect('votes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function done()
    {
        return view('votes.done');
    }
}
