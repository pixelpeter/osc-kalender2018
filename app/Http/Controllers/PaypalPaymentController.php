<?php

namespace App\Http\Controllers;


use App\Facades\PaymentLog;
use App\Http\Requests;
use App\Models\Order;
use App\Models\OscMember;
use Auth;
use Mail;
use Paypalpayment;
use PPConnectionException;

class PaypalPaymentController extends Controller
{
    /**
     * @var OscMember
     */
    private $member;
    /**
     * @var Order
     */
    private $order;

    public function __construct(OscMember $member, Order $order)
    {
        $config = config('paypal_payment'); // Get all config items as multi dimensional array
        $flatConfig = array_dot($config); // Flatten the array with dots

        try {
            $this->_apiContext = Paypalpayment::ApiContext(
                $flatConfig['account.ClientId'],
                $flatConfig['account.ClientSecret']);

            $this->_apiContext->setConfig($flatConfig);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo "Exception: " . $ex->getMessage() . PHP_EOL;
            var_dump(json_decode($ex->getCode()), json_decode($ex->getData()));
            exit(1);
        }

        $this->member = $member;
        $this->order = $order;
    }


    /*
    * Process payment using credit card
    */
    public function create()
    {
        $member = $this->member->with(['address'])->find(Auth::id());

        $order = $this->order
            ->where('member_id',Auth::id())
            ->where('payment_id','')
            ->orderBy('created_at','DESC')
            ->first();

        //        var_dump(
        //            config('calendarapp.price'),
        //            config('calendarapp.price')*0.19,
        //            $member->order->shipping,
        //            $member->order->shipping*0.19,
        //            config('calendarapp.price')*$member->order->count,
        //            (config('calendarapp.price')*0.19)*$member->order->count,
        //            ((config('calendarapp.price')*0.19)*$member->order->count)+($member->order->shipping*0.19),
        //            (config('calendarapp.price')*$member->order->count)+$member->order->shipping+((config('calendarapp.price')*0.19)*$member->order->count)+($member->order->shipping*0.19)
        //        );

        $itemPrice = round(config('calendarapp.price'),2);
        $itemPriceTax = round(config('calendarapp.price')*0.19,2);

        $shippingPrice = round($order->shipping,2);
        $shippingPriceTax = round($shippingPrice*0.19,2);

        $subTotal = round($itemPrice*$order->count,2);
        $subTotalTax = round($itemPriceTax*$order->count,2);

        $totalTax = $subTotalTax+$shippingPriceTax;
        $total = $subTotal+$shippingPrice+$totalTax;

        //        dd(
        //            $itemPrice,
        //            $itemPriceTax,
        //            $shippingPrice,
        //            $shippingPriceTax,
        //            $subTotal,
        //            $subTotalTax,
        //            $totalTax,
        //            $total
        //        );

        // ### Address
        $addr = Paypalpayment::address();
        $addr->setLine1($member->address->street);
        $addr->setCity($member->address->city);
        $addr->setPostalCode($member->address->zip);
        $addr->setCountryCode($member->address->country);

        // ### Shipping Address
        $shippingAddr = Paypalpayment::shippingAddress();
        $shippingAddr->setRecipientName($member->address->first_name.' '.$member->address->last_name);
        $shippingAddr->setLine1($member->address->street);
        $shippingAddr->setCity($member->address->city);
        $shippingAddr->setPostalCode($member->address->zip);
        $shippingAddr->setCountryCode($member->address->country);

        // ### Payer
        $payerInfo = Paypalpayment::payerInfo();
        $payerInfo->setFirstName($member->address->first_name);
        $payerInfo->setLastName($member->address->last_name);
        $payerInfo->setBillingAddress($addr);
        $payerInfo->setShippingAddress($shippingAddr);

        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod('paypal');
        $payer->setPayerInfo($payerInfo);

        // ### Item
        $item1 = Paypalpayment::item();
        $item1->setName(config('calendarapp.product'))
            ->setCurrency('EUR')
            ->setQuantity($order->count)
            ->setTax($itemPriceTax)
            ->setPrice($itemPrice); // NETTO

        // ### Item ist
        $itemList = Paypalpayment::itemList();
        $itemList->setItems([$item1]);
        $itemList->setShippingAddress($shippingAddr);


        // ### Payment Details
        $details = Paypalpayment::details();
        $details->setShipping($shippingPrice)
            ->setTax($totalTax)
            //total of items prices
            ->setSubtotal($subTotal);

        // ### Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("EUR")
            ->setTotal($total)
            ->setDetails($details);

        // ### Transaction
        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList);

        // ### Payment
        $payment = Paypalpayment::payment();
        $payment->setIntent("sale")
            ->setRedirectUrls([
                'return_url' => config('calendarapp.return_url'),
                'cancel_url' => config('calendarapp.cancel_url'),
            ])
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {
            // ### Create Payment
            $payment->create($this->_apiContext);
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            PaymentLog::write("State:", $payment->getState());
            PaymentLog::write("Info:", $payment->toJson());
            PaymentLog::write("Exception:", $ex->getData());

            $order->payment_status = 'error';
            $order->payment_info = $payment->toJson();
            $order->save();

            $data = json_decode($ex->getData());
            $message = $data->message;
            $details = array_pluck($data->details,'issue');

            $mailData  = $payment->toJson();
            $mailData .= "<hr>";
            $mailData .= $ex->getData();

            Mail::raw($mailData, function($message)
            {
                $message->from(config('calendarapp.email_from'));
                $message->to(config('calendarapp.email_from'));
                $message->subject('PaymentError create ['.getenv('APP_ENV').']');
            });

            return response()->view('errors.custom', compact('message', 'details'), 500);
        }

        $id = $payment->getId();
        $state = $payment->getState();

        PaymentLog::write("PaymentId:", $id);
        PaymentLog::write("State:", $state);
        PaymentLog::write("Info:", $payment->toJson());

        $order->payment_id = $id;
        $order->payment_status = $state;
        $order->payment_info = $payment->toJson();
        $order->save();

        // dd($id, $state, $order, $payment);
        return redirect('payment/' . $id);
    }

    public function show($id)
    {
        $payment = Paypalpayment::getById($id, $this->_apiContext);
        $approvalUrl = $payment->getApprovalLink();

        return view('payment.wall')->with('approvalUrl', $approvalUrl);
    }


    public function execute()
    {
        $id = \Input::get('paymentId');
        $payerID = \Input::get('PayerID');

        $order = $this->order
            ->where('payment_id',$id)
            ->first();

        try {
            $payment = Paypalpayment::getById($id, $this->_apiContext);

            $execution = Paypalpayment::PaymentExecution();
            $execution->setPayerId($payerID);

            //Execute the payment
            $payment->execute($execution, $this->_apiContext);

            PaymentLog::write("PaymentId:", $id);
            PaymentLog::write("State:", $payment->getState());
            PaymentLog::write("Info:", $payment->toJson());

            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            $sale_status = $sale->getState();

            $order->payment_status = $payment->getState();
            $order->payment_info = $payment->toJson();
            $order->sale_status = $sale_status;
            $order->save();

            return redirect('order/result/'.$id);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            PaymentLog::write("PaymentId:", $id);
            PaymentLog::write("State:", $payment->getState());
            PaymentLog::write("Info:", $payment->toJson());
            PaymentLog::write("Exception:", $ex->getData());

            $order->payment_status = $payment->getState();
            $order->payment_info = $payment->toJson();
            $order->save();

            $data = json_decode($ex->getData());
            $message = $data->message;
            $details = array_pluck($data->details,'issue');

            Mail::raw($ex->getData(), function($message) use($id)
            {
                $message->from(config('calendarapp.email_from'));
                $message->to(config('calendarapp.email_from'));
                $message->subject('PaymentError execute '.$id);
            });

            return response()->view('errors.custom', compact('message', 'details'), 500);
        }
    }

    public function cancel()
    {
        $order = $this->order
            ->where('member_id',Auth::id())
            ->where('payment_status','created')
            ->latest()
            ->first();

        $order->payment_status = "canceled";
        $order->save();

        return redirect('order/canceled');
    }
}
