<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Member;
use App\Models\Photo;

class TestController extends Controller
{
    protected $member;

    /**
     * @param Member $member
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    public function index()
    {
        $cal = 12.61;
        $ship = 4.20;

        for($i=1;$i<=5;$i++)
        {
            $total1 = ($cal*$i)+$ship;
            $total2 = floor($total1*1.19);
            echo "$i|$total1|$total2<br>";
        }


    }
}
