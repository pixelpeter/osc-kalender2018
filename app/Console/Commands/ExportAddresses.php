<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Mail;

class ExportAddresses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export-addresses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export addresses';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // GET AVAILABLE COUNTRIES
        $countries = Order::notExported()->distinct()->get(['country']);
        $this->info($countries->pluck('country'));


        $country = $this->ask('Country');

        // GET AVAILABLE SKUs
        $counts = Order::notExported()->whereCountry($country)->distinct()->get(['count']);
        $this->info($counts->pluck('count'));

        $sku = $this->ask('Sku');

        $orders = Order::notExported()
            ->where('country', $country)
            ->where('count', $sku)
            ->take(24)->get();

        $filename = sprintf('OSC_2018_%s_%s_%s.csv',
            date('YmdHis'),
            $country,
            $sku
        );

        if ( !file_exists(storage_path('addresses'))) {
            mkdir(storage_path('addresses'));
        }

        $fh = fopen(storage_path('addresses/'.$filename), 'w+');

        // write header
        fputcsv($fh, ['NAME', 'ZUSATZ', 'STRASSE', 'NUMMER', 'PLZ', 'STADT', 'LAND', 'ADRESS_TYP', 'COUNT'], ';');
        fputcsv($fh, ['Free Castle Trade H&H UG', '', 'Immentalstr.', '1', '79104', 'Freiburg', 'DE', 'HOUSE', '0'], ';');

        // write records
        foreach($orders as $order)
        {
            fputcsv($fh, [
                sprintf('%s %s', trim($order->first_name), trim($order->last_name)),
                $order->company,
                $order->street,
                '',
                $order->zip,
                $order->city,
                $order->country,
                'HOUSE',
                $order->count
            ], ';');

            $order->exported_at = date('Y-m-d H:i:s');
            $order->save();
        }

        fclose($fh);

        exec("sed -i 's/\"//g' ".storage_path('addresses/'.$filename));

        Mail::raw('OSC Address Export '.$filename, function ($message) use ($filename){
            $message->subject('OSC Address Export '.$filename);
            $message->from('pp@opel-speedster-club.de');
            $message->to('info@pixelpeter.de');

            $message->attach(
                storage_path('addresses/'.$filename)
            );

        });

        $this->info(
            sprintf('Exported %s addresses', $orders->count())
        );
    }
}
