<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ThumbnailMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thumbnails:make:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Thumbnails.';
    /**
     * @var \App\Services\ThumbnailMaker
     */
    private $thumbnailmaker;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\App\Services\ThumbnailMaker $thumbnailmaker)
    {
        $this->thumbnailmaker = $thumbnailmaker;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->thumbnailmaker->makeThumbnail($this->argument('image'));
        $this->thumbnailmaker->generateAll();
    }
}
