<?php

namespace App\Providers;

use App\Models\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('maxPhotos','App\Validators\MaxPhotos@validate');
        Validator::replacer('maxPhotos','App\Validators\MaxPhotos@message');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Models\Member::class,
            \App\Models\OscMember::class
        );
    }
}
