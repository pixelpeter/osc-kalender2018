<?php namespace App\Providers;

use App\Auth\IbfUserProvider;
use Illuminate\Support\ServiceProvider;

class IbfAuthServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->extend('ibf', function () {
            $model = $this->app['config']['auth.model'];

            return new IbfUserProvider($model);
        });
    }

    public function register()
    {
        return;
    }

}
