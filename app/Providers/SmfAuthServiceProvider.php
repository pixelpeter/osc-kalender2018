<?php namespace App\Providers;

use App\Auth\SmfUserProvider;
use Illuminate\Support\ServiceProvider;

class SmfAuthServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->extend('smf', function () {
            $model = $this->app['config']['auth.model'];

            return new SmfUserProvider($model);
        });
    }

    public function register()
    {
        return;
    }

}
