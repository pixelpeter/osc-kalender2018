<?php namespace App\Auth;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class IbfUserProvider extends EloquentUserProvider
{
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        // $name = mb_convert_encoding($credentials['name'],'ISO-8859-1', 'UTF-8');
        $name = $credentials['name'];
        // dd($name, $credentials);
        $query = $this->createModel()->newQuery();
        $query->join('ibf_members_converge', "ibf_members.id", "=", "ibf_members_converge.converge_id");
        $query->select(
            'ibf_members.id',
            'ibf_members.name',
            'ibf_members_converge.converge_pass_hash',
            'ibf_members_converge.converge_pass_salt'
        );
        $query->where('name', $name);

        return $query->first();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $pass = mb_convert_encoding($credentials['password'],'ISO-8859-1', 'UTF-8');

        $hash = md5(md5($user->converge_pass_salt) . md5($pass));

        // \DebugBar::info($credentials, $user, $user->converge_pass_hash, $hash );
        return $user->converge_pass_hash === $hash;
    }

}