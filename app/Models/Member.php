<?php namespace App\Models;


interface Member
{
    public function getMemberNameAttribute();

    public function getMemberIdAttribute();

}