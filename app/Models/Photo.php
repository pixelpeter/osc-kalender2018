<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kalender2018_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'member_id', 'is_censored'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function scopeNotVoted($query)
    {
        return  $query->where('is_censored',0)
            ->whereNotIn('id', function($query) {
                $query->from('kalender2018_votes')
                    ->selectRaw('photo_id')
                    ->where('member_id', \Auth::id());
                })
            ->orderByRaw("RAND()")
            ->firstOrFail();
    }
}
