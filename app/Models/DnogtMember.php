<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class DnogtMember extends Model implements Member, AuthenticatableContract
{
    use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'smf_members';

    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'forum';

    /**
     * The primary key for this model.
     *
     * @var string
     */
    protected $primaryKey = 'id_member';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Disable the token for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // not supported
    }

    public function getMemberNameAttribute()
    {
        return $this->member_name;
    }

    public function getMemberIdAttribute()
    {
        return $this->id_member;
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\Photo::class,'member_id');
    }
}
