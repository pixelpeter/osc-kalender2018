<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OscAddress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ibf_pfields_content';

    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'forum';

    public $timestamps = false;

    protected $primaryKey = 'member_id';

    protected $fillable = ['first_name', 'last_name', 'company', 'street', 'zip', 'city', 'country'];

    public function member()
    {
        return $this->belongsTo(\App\Models\OscMember::class,'member_id');
    }

    public function getFirstNameAttribute()
    {
        return $this->field_16;
    }

    public function getLastNameAttribute()
    {
        return $this->field_15;
    }

    public function getCompanyAttribute()
    {
        return $this->field_18;
    }

    public function getStreetAttribute($value)
    {
        return $this->field_6;
    }

    public function getZipAttribute($value)
    {
        return $this->field_7;
    }

    public function getCityAttribute($value)
    {
        return $this->field_8;
    }

    public function getCountryAttribute($value)
    {
        return $this->field_10 ?: "DE";
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['field_16'] = $value;
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['field_15'] = $value;
    }

    public function setCompanyAttribute($value)
    {
        $this->attributes['field_18'] = $value;
    }

    public function setStreetAttribute($value)
    {
        $this->attributes['field_6'] = $value;
    }

    public function setZipAttribute($value)
    {
        $this->attributes['field_7'] = $value;
    }

    public function setCityAttribute($value)
    {
        $this->attributes['field_8'] = $value;
    }

    public function setCountryAttribute($value)
    {
        $this->attributes['field_10'] = $value;
    }
}
