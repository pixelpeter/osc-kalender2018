<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kalender2018_orders';

    protected $fillable = [
        'member_id',
        'company', 'first_name', 'last_name', 'street', 'zip', 'city', 'country', 'email',
        'count', 'shipping', 'product_total', 'total'
    ];

    protected $dates = ['exported_at'];

    public function member()
    {
        return $this->belongsTo(\App\Models\Member::class);
    }

    public function scopeNotExported($query)
    {
        $query->whereNull('exported_at')->where('sale_status', 'completed');
    }
}
