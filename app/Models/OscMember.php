<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class OscMember extends Model implements Member, AuthenticatableContract
{
    use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ibf_members';

    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'forum';

    /**
     * The primary key for this model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Disable the token for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // not supported
    }

    public function getMemberNameAttribute()
    {
        return $this->name;
    }

    public function getMemberIdAttribute()
    {
        return $this->id;
    }

    public function getFirstNameAttribute()
    {
        return $this->address->firstName;
    }

    public function getLastNameAttribute()
    {
        return $this->address->lastName;
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\Photo::class,'member_id');
    }

    public function address()
    {
        return $this->hasOne(\App\Models\OscAddress::class,'member_id');
    }

    public function order()
    {
        return $this->hasOne(\App\Models\Order::class, 'member_id');
    }
}
