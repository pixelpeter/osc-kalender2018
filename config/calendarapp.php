<?php

return [
    'allowedIds' => [1],
    'product' => getenv('APP_PRODUCT_NAME'),
    'email_from' => getenv('APP_EMAIL_FROM'),
    'return_url' => getenv('APP_RETURN_URL'),
    'cancel_url' => getenv('APP_CANCEL_URL'),
    'price' => 17.50/1.19,
    'shipping' => [
        'DE' => [
            1 => 5.00/1.19,
            2 => 5.00/1.19,
            3 => 5.00/1.19,
            4 => 6.00/1.19,
            5 => 6.00/1.19
        ],
        'CH' => [
            1 => 7.00/1.19,
            2 => 27.00/1.19,
            3 => 27.00/1.19,
            4 => 27.00/1.19,
            5 => 27.00/1.19
        ],
        'EU' => [
            1 => 7.00/1.19,
            2 => 9.00/1.19,
            3 => 9.00/1.19,
            4 => 16.00/1.19,
            5 => 16.00/1.19
        ]
    ]
];