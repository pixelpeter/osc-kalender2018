<?php

return array(
	# Account credentials from developer portal
	'account' => array(
		'ClientId' => getenv('PAYPAL_CLIENT_ID'),
		'ClientSecret' => getenv('PAYPAL_CLIENT_SECRET'),
	),

	# Connection Information
	'http' => array(
		'ConnectionTimeOut' => 90,
		'Retry' => 1,
		//'Proxy' => 'http://[username:password]@hostname[:port][/path]',
	),

	# Service Configuration
	'service' => array(
		# For integrating with the live endpoint,
		# change the URL to https://api.paypal.com!
		'EndPoint' => getenv('PAYPAL_ENDPOINT'),
	),

	'mode' => getenv('PAYPAL_ENV'),


	# Logging Information
	'log' => array(
		'LogEnabled' => true,
		'LoggingEnabled' => true,

		# When using a relative path, the log file is created
		# relative to the .php file that is the entry point
		# for this request. You can also provide an absolute
		# path here
		'FileName' => storage_path().'/logs/PayPal_'.date('Y-m-d').'.log',

		# Logging level can be one of FINE, INFO, WARN or ERROR
		# Logging is most verbose in the 'FINE' level and
		# decreases as you proceed towards ERROR
		'LogLevel' => 'FINE',
	),
);
