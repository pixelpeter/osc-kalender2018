<?php

return [
    'DE' => 'Deutschland',
    'AT' => 'Österreich',
    'CH' => 'Schweiz',
    'NL' => 'Niederlande',
    'BE' => 'Belgien',
    'FR' => 'Frankreich',
    'IT' => 'Italien',
    'SE' => 'Schweden',
    'ES' => 'Spanien',
    'GB' => 'Vereinigtes Königreich Großbritannien und Nordirland',
    'LT' => 'Litauen',
];