@extends('layouts.main')

@section('content')
    <script src="https://www.paypalobjects.com/webstatic/ppplus/ppplus.min.js" type="text/javascript"></script>

    <div class="container order">
        {!! Form::open([ 'url' => 'order/update', 'method' => 'post'  ]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
                <ul class='nav nav-wizard'>

                    <li><a href="/order" >1. Adresse</a></li>

                    <li><a href='/order/show'>2. Bestellung</a></li>

                    <li class='active'><a href='#'>3. Zahlung</a></li>

                    <li><a>4. Übersicht</a></li>

                </ul>

                <h3>Zahlung</h3>

                <div id="ppplus"></div>

                <script type="application/javascript"> var ppp = PAYPAL.apps.PPP({
                        "approvalUrl": "<?=$approvalUrl?>",
                        "placeholder": "ppplus",
                        "mode": "<?=getenv('PAYPAL_ENV')?>",
                        "country": "<?=\Session::get('country')?>",
                        "language": "de_DE"
                    });
                </script>

            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection