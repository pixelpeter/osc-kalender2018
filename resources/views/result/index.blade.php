@extends('layouts.main')


@section('content')
<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header">Photos</h1>
    </div>

    @foreach($photos as $photo)
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <div class="pull-left" style="margin-left: 25px">
                {{ $photo->id }} [{{ $photo->vote }}]
            </div>
            <div class="pull-right" style="margin-right: 25px">
                <a href="/censor/{{ $photo->id }}/1">forbiddden</a> |
                <a href="/censor/{{ $photo->id }}/2">dupe LastYear</a> |
                <a href="/censor/{{ $photo->id }}/3">dupe ThisYear</a>
            </div>
            <a class="thumbnail" href="/uploads/org/{{ $photo->name }}" target="_blank">
                <img class="img-responsive" src="/uploads/thumbnail/{{ $photo->name }}" alt="">
            </a>
        </div>
    @endforeach

</div>
@endsection
