@extends('layouts.main')


@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
        <ul class='nav nav-wizard'>

            <li>1. Adresse</li>

            <li>2. Bestellung</li>

            <li>3. Zahlung</li>

            <li>4. Übersicht</li>

        </ul>

        <h3>Fehler</h3>

        <div class="alert alert-danger" role="alert">
            <strong>FEHLER</strong>
            {!! $message !!}
            <ul>
                @foreach($details AS $detail)
                <li>{{ $detail }}</li>
                @endforeach
            </ul>

        </div>
        <p>
            <a href="/order">&raquo; Nochmals probieren</a>
        </p>
    </div>
</div>

@endsection
