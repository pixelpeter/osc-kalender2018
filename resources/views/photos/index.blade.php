@extends('layouts.main')


@section('content')
<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header">Photos</h1>
    </div>

    @foreach($photos as $photo)
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                {{ $photo->id }}
                <img class="img-responsive" src="/uploads/thumbnail/{{ $photo->name }}" alt="">
            </a>
        </div>
    @endforeach

</div>
@endsection
