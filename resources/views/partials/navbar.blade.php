<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top navbar-shrink">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="/">Kalender 2018</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li {{ (Request::is('/') ? 'class=active' : '') }}>
                    <a href="/">Home</a>
                </li>
                @if ( Auth::user() )
                    <li><a href="/logout">Log out</a></li>
                @elseif( false )
                    <li {{ (Request::is('login') ? 'class=active' : '') }}><a href="/login">Log in</a></li>
                @endif

                @if ( 1 == 2 && Auth::user() )
                    <li {{ (Request::is('order') ? 'class=active' : '') }}>
                        <a href="/order">Bestellung</a>
                    </li>
                @else
                    <li class="inactive">
                        <a href="#">Bestellung</a>
                    </li>
                @endif


                @if ( 1 == 2 && Auth::user() )
                    <li {{ (Request::is('upload') ? 'class=active' : '') }}>
                        <a href="/upload">Bilder hochladen</a>
                    </li>
                @else
                    <li class="inactive">
                        <a href="#">Bilder hochladen</a>
                    </li>
                @endif

                @if ( 1 == 2 && Auth::user())
                    <li {{ (( Request::is('votes') || Request::is('votes/done') ) ? 'class=active' : '') }}>
                        <a href="/votes">Bilder bewerten</a>
                    </li>
                @else
                    <li class="inactive">
                        <a href="#" >Bilder bewerten</a>
                    </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
