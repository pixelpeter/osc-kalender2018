@if ( count($errors) )

    <div class="alert alert-danger" role="alert">
        <strong>FEHLER</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif