@extends('layouts.main')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-3 text-center">
                <h2 class="section-heading">Bilder hochladen</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <form action="/upload" class="dropzone form-control">
                    {!! csrf_field() !!}
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-push-3 text-center">
                <h4 class="section-heading">Deine bereits hochgeladenen Bilder</h4>
                <h3 class="section-subheading section-subheading-upload text-muted">Falls keine Bilder angezeigt werden die Seite <a href="/upload">neu laden</a></h3>
            </div>
        </div>

        <div class="row">
            @foreach($photos as $photo)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <img class="thumbnail img-responsive" src="/uploads/thumbnail/{{ $photo->name }}" alt="">
                    {!! Form::open(['url' => 'upload/'.($photo->id), 'method' => 'delete']) !!}
                    <input type="submit" value="X" class="btn btn-danger img-delete" aria-label="Close"></input>
                    {!! Form::close() !!}
                </div>
            @endforeach
        </div>
    </div>

@endsection