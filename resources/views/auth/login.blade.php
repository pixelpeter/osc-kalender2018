@extends('layouts.main')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-3 text-center">
                <h2 class="section-heading">Login</h2>
                <h3 class="section-subheading text-muted">Bitte einloggen um Bilder hochladen und bewerten zu können.</h3>
                @include('partials.errors')
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <form name="sentMessage" id="contactForm" method="POST" action="/login" novalidate>
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Benutzername" id="name" name="name" required data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Passwort" id="password" name="password" required data-validation-required-message="Please enter your email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="submit" class="btn btn-xl">Login</button>
                </form>
            </div>
        </div>
    </div>

@endsection

