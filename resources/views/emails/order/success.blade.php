Hallo {{ $member->address->first_name }} {{ $member->address->last_name }}

<h2>Bestellbestätigung</h2>
<p>Bestell ID: {{ $order->id }}</p>

<table class="table">
    <thead>
    <th>#</th>
    <th>Artikel</th>
    <th>Anzahl</th>
    <th>Preis</th>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>{{ $product }}</td>
        <td>{{ $order->count }}</td>
        <td align="right">{{ number_format($order->product_total,2) }} €</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Versandkosten {{ $order->country }}</td>
        <td>1</td>
        <td align="right">{{ number_format($order->shipping,2) }} €</td>
    </tr>
    <tr>
        <td colspan="4"><hr></td>
    </tr>
    <tr>
        <td></td>
        <td>Zwischensumme</td>
        <td></td>
        <td align="right">{{ number_format($order->total,2) }} €</td>
    </tr>
    <tr>
        <td></td>
        <td>19% MwSt.</td>
        <td></td>
        <td align="right">{{ number_format($tax,2) }} €</td>
    </tr>
    <tr>
        <td colspan="4"><hr></td>
    </tr>
    <tr>
        <td></td>
        <td><strong>Gesamt</strong></td>
        <td></td>
        <td align="right"><strong>{{ number_format($total_wt,2) }} €</strong></td>
    </tr>
    </tbody>
</table>

<h2>Bezahlung</h2>
<p>Deine Transaktion war: {{ $order->payment_status }}</p>
<p></p>ID: {{ $order->payment_id }}</p>

<h2>Versand</h2>
<p>Wir versenden an folgende Adresse</p>
{{ $order->company }}<br>
{{ $order->first_name }} {{ $order->last_name }}<br>
{{ $order->street }}<br>
{{ $order->zip }} {{ $order->city }}<br>
{{ $country }}<br>
<br><br>
<p>Vielen Dank für Deine Bestellung<br>
Peter
</p>
