Hallo {{ $member->address->first_name }} {{ $member->address->last_name }}


<h2>Probleme bei Deiner Bestellung</h2>
<p>Leider gab es bei Deiner Bestellung mit der Bestell ID: {{ $order->id }} ein Problem<br>
<b>Es konnte keine erfolgreiche Zahlung/Transaktion festgestelt werden</b>
</b>
<p>Bitte wende Dich an den Administrator oder probiere es noch einmal</p>
<br>
<p>Vielen Dank für Deine Bestellung<br>
    Peter
</p>
