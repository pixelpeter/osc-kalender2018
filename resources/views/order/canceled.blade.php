@extends('layouts.main')


@section('content')

    <div class="container order">
        {!! Form::open([ 'url' => 'order/update', 'method' => 'post'  ]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
                <ul class='nav nav-wizard'>

                    <li>1. Adresse</li>

                    <li>2. Bestellung</li>

                    <li>3. Zahlung</li>

                    <li class='active'><a>4. Übersicht</a></li>

                </ul>

                <h3>Übersicht</h3>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Zahlung</h3>
                        </div>
                        <div class="panel-body panel-body-order">
                            Du hast die Zahlung abgebrochen.<br>
                            Die Bestellung wurde daher gelöscht und wird <b>NICHT</b> ausgeführt.
                        </div>
                    </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection