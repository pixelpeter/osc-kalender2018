@extends('layouts.main')


@section('content')

    <div class="container order">
        {!! Form::open([ 'url' => 'order/store', 'method' => 'post'  ]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
                <ul class='nav nav-wizard'>

                    <li class="active"><a href='#step1'>1. Adresse</a></li>

                    <li>2. Bestellung</li>

                    <li>3. Zahlung</li>

                    <li>4. Übersicht</li>
                </ul>

                <h3>Adresse</h3>
                <!--- Firstname Field --->
                <div class="form-group">
                    {!! Form::label('first_name', 'Vorname  :') !!}
                    {!! Form::text('first_name', $member->firstName, ['class' => 'form-control']) !!}
                </div>

                <!--- Lastname Field --->
                <div class="form-group">
                    {!! Form::label('last_name', 'Nachname:') !!}
                    {!! Form::text('last_name', $member->lastName, ['class' => 'form-control']) !!}
                </div>

                <!--- Firma/Zusatz Field --->
                <div class="form-group">
                    {!! Form::label('company', 'Firma/Zusatz:') !!}
                    {!! Form::text('company', $member->address->company, ['class' => 'form-control']) !!}
                </div>

                <!--- Street Field --->
                <div class="form-group">
                    {!! Form::label('street', 'Straße:') !!}
                    {!! Form::text('street', $member->address->street, ['class' => 'form-control']) !!}
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <!--- PLZ Field --->
                        <div class="form-group">
                            {!! Form::label('zip', 'PLZ:') !!}
                            {!! Form::text('zip', $member->address->zip, ['class' => 'form-control']) !!}
                        </div>        
                    </div>
                    
                    <div class="col-sm-8">
                        <!--- Stadt Field --->
                        <div class="form-group">
                            {!! Form::label('city', 'Stadt:') !!}
                            {!! Form::text('city', $member->address->city, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <!--- Land   Field --->
                <div class="form-group">
                    {!! Form::label('country', 'Land:') !!}
                    {!! Form::select('country', $countries, $member->address->country,['class' => 'form-control']) !!}
                </div>

                <!--- E-Mail Field --->
                <div class="form-group">
                    {!! Form::label('email', 'E-Mail:') !!}
                    {!! Form::email('email', $member->email, ['class' => 'form-control']) !!}
                </div>
                
                <!--- Submit Field --->
                <div class="form-group">
                    {!! Form::submit('Weiter', ['class' => 'btn btn-primary']) !!}
                </div>
                

                
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection