@extends('layouts.main')


@section('content')

    <div class="container order">
        {!! Form::open([ 'url' => 'order/update', 'method' => 'post'  ]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
                <ul class='nav nav-wizard'>

                    <li><a href="/order" >1. Adresse</a></li>

                    <li class='active'><a href='#'>2. Bestellung</a></li>

                    <li>3. Zahlung</li>

                    <li>4. Übersicht</li>

                </ul>

                <h3>Bestellung</h3>

                <!--- Land   Field --->
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-2">
                        {!! Form::select('sku', array_combine(range(1,5),range(1,5)), \Session::get('sku'),['class' => 'form-control']) !!}
                        </div>
                        <div class="col-xs-10">
                            <h4>{{ $product }}</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table">
                                <thead>
                                    <th>#</th>
                                    <th>Artikel</th>
                                    <th>Anzahl</th>
                                    <th>Preis</th>
                                    <th>Gesamt</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>{{ $product }}</td>
                                        <td>{{ \Session::get('sku') }}</td>
                                        <td>{{ $price_wt }} €</td>
                                        <td>{{ $product_total_wt }} €</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Versandkosten {{ \Session::get('country') }}</td>
                                        <td>1</td>
                                        <td></td>
                                        <td>{{ $shipping_wt }} €</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>enthaltene MwSt.</td>
                                        <td>{{ $tax }} €</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><strong>Gesamt</strong></td>
                                        <td><strong>{{ $total_wt }} €</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <!--- Submit Field --->
                <div class="form-group">
                    {!! Form::submit('Aktualisieren', ['name' => 'update', 'class' => 'btn btn-primary']) !!}
                    {!! Form::submit('Zahlungspflichtig bestellen', ['name' => 'create', 'class' => 'btn btn-primary pull-right']) !!}
                </div>
                

                
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection