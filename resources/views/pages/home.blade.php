@extends('layouts.main')

@section('content')

    <div class="container home">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Liebe Club Mitglieder</h2>

                <h3 class="section-subheading">Auch dieses Jahr starten wir nun zum <strong>dreizehnten</strong>
                    Mal eine Kalenderaktion auf Basis Eurer eingesendeten Bilder.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4>Bilder-Upload</h4>

                <p>
                    In der Zeit vom <strong>Mittwoch, 01.11.2017 8:00 - Dienstag, 14.11.2017 23:59</strong> könnt Ihr
                    über diese Website Eure Bilder zur Bewertung (durch die Mitglieder) einsenden.
                </p>

                <p>
                    Zum Hochladen müsst Ihr Euch natürlich vorher einloggen!
                </p>

                <p>
                    Es sind maximal <strong>5 Bilder</strong> pro Mitglied erlaubt. Also bitte Qualität statt Quantität
                    :-))
                </p>

                <p>
                    Während dieser Zeit ist noch <strong>keine</strong> Bewertung der Bilder möglich. Dies folgt in
                    Phase II.
                </p>
            </div>

            <div class="col-md-4">
                <h4>Bilder-Bewertung</h4>

                <p>
                    Im Zeitraum vom <strong>Mittwoch, 15.11.2017 12:00 - Mittwoch, 22.11.2017 09:00</strong> könnt
                    Ihr Eure Bewertungen für die eingesendeten Bilder abgeben.
                </p>

                <p>
                    Ein weiteres Hochladen von Bildern ist während dieser Phase <strong>nicht mehr möglich</strong>.
                </p>
            </div>

            <div class="col-md-4">
                <h4>Bilder-Sortierung</h4>

                <p>
                    Wie auch im letzen Jahr wird die Sortierung (Zuweisung zum Monat) der gewählten Bilder durch das
                    Opel-Speedster-Club Team durchgeführt.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <h4>Die Regeln</h4>
            </div>
        </div>

        <div class="row rules">
            <div class="col-md-6">
                <p>1
                    Der Hauptinhalt des Bildes soll aus einem oder mehreren GTs bestehen. Reine Landschaftsbilder
                    oder Personen/Portraits sind nicht erlaubt. Das Bild muss im <strong>Querformat</strong> vorliegen.
                </p>

                <p>2
                    Das Bild muss <strong>mindestens</strong> eine Größe/Auflösung von <strong>2500 x 1500</strong>
                    Pixeln besitzen.
                </p>

                <p>3
                    Das Bild bitte möglichst <strong>ohne Kompression</strong> (max 90% bei JPG-Dateien) abspeichern.
                </p>

                <p>4
                    Bild-Retouchen sind nicht erlaubt. Hiervon ausgenommen sind Farb-/Kontrastkorrekturen oder
                    Staubentfernung.
                </p>

                <p>6
                    Aus mehrfache Nachfrage: HDR-Bilder sind erlaubt
                </p>
            </div>

            <div class="col-md-6">
                <p>6
                    Bei ähnlichen Motiven behält sich das opel-GT-forum-Team vor Bilder aus der Wertung zu nehmen.
                </p>

                <p>7
                    Durch das Hochladen bestätigt Ihr, dass Ihr der Rechteinhaber an dem Bild seit, die o.g. Regeln
                    akzeptiert und mit einer Veröffentlichung einverstanden seid.
                </p>

                <p>8
                    Bilder die bereits in einem Kalender der vorherigen Jahre publiziert wurden werden aus der Wertung genommen.
                </p>
                <p>9
                    Das opel-GT-forum-Team behält sich vor Bilder welche den Regeln nicht entsprechen <strong>aus
                        der
                        Wertung zu nehmen</strong>.
                </p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <h4>Die vorherigen Kalender</h4>

                <p>
                    Für eine Übersicht über die bereits in den vorherigen Kalendern verwendeten Motive könnt Ihr die
                    "alten" Kalenderseiten aufrufen
                </p>
            </div>
        </div>
        <!--
        <div class="row">
            <div class="col-xs-6">
                <ul class="inline-list">
                    <li><a href="http://kalender2017.opelspeedster.club" target="_blank">Kalender 2017</a></li>
                    <li><a href="http://kalender2015.opel-speedster-club.de" target="_blank">Kalender 2015</a></li>
                    <li><a href="http://kalender2014.opel-speedster-club.de" target="_blank">Kalender 2014</a></li>
                    <li><a href="http://kalender2013.opel-speedster-club.de" target="_blank">Kalender 2013</a></li>
                    <li><a href="http://kalender2012.opel-speedster-club.de" target="_blank">Kalender 2012</a></li>
                    <li><a href="http://kalender2011.opel-speedster-club.de" target="_blank">Kalender 2011</a></li>
                    <li><a href="http://kalender2010.opel-speedster-club.de" target="_blank">Kalender 2010</a></li>
                </ul>
            </div>
            <div class="col-xs-6">
                <ul class="inline-list">
                    <li><a href="http://kalender2009.opel-speedster-club.de" target="_blank">Kalender 2009</a></li>
                    <li><a href="http://kalender2008.opel-speedster-club.de" target="_blank">Kalender 2008</a></li>
                    <li><a href="http://kalender2007.opel-speedster-club.de" target="_blank">Kalender 2007</a></li>
                    <li><a href="http://kalender2006.opel-speedster-club.de" target="_blank">Kalender 2006</a></li>
                    <li><a href="http://kalender2005.opel-speedster-club.de" target="_blank">Kalender 2005</a></li>
                    <li><a href="http://kalender2004.opel-speedster-club.de" target="_blank">Kalender 2004</a></li>
                </ul>
            </div>
        </div>
        -->
    </div>

@endsection