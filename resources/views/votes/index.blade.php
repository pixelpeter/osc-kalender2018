@extends('layouts.main')

@section('content')
    <div class="row row_vote">
        <div class="col-xs-12 text-center">
            <h1>Bilder bewerten <small>{{ $votesCount }} von {{ $photoCount }}</small></h1>
            <div class="text-center" style="width: 100%">
                <div class="btn-group votes_btn-group" role="group">
                    @foreach(range(1,6) as $vote)
                        <a class="btn btn-default vote-{{ $vote }}"
                           href="/votes/save/{{ Crypt::encrypt([
                            'member_id' => Auth::id(),
                            'photo_id' => $photo->id,
                            'vote' => $vote]) }}">{{ $vote-1 }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <img src="/uploads/detail/{{ $photo->name }}" class="img-responsive img-responsive_vote" />
        </div>
    </div>
@endsection



