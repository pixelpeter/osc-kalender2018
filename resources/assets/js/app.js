// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

$('.img-delete').click(function(e) {
    return confirm("Bild wirklich löschen?");
});