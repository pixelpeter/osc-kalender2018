var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'dropzone.css',
        'bootstrap.css',
        'base.css',
        'button.css',
        'nav.css',
        'section.css',
        'form.css',
        'upload.css',
        'order.css'
    ]);

    mix.scripts([
       'jquery.js',
        'bootstrap.js',
        'dropzone.js',
        'app.js',
        'ie10-viewport-bug-workaround.js'
    ]);
});
