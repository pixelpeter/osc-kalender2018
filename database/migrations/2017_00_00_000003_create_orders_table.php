<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kalender2018_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id');
            $table->string('company');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('street');
            $table->string('zip');
            $table->string('city');
            $table->string('country');
            $table->string('email');

            $table->integer('count');
            $table->decimal('shipping',8,4);
            $table->decimal('product_total',8,4);
            $table->decimal('total',8,4);

            $table->string('payment_id');
            $table->string('payment_status');
            $table->text('payment_info');

            $table->timestamps();
        });

        \DB::update("ALTER TABLE kalender2018_orders AUTO_INCREMENT = 121212;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kalender2018_orders');
    }
}
